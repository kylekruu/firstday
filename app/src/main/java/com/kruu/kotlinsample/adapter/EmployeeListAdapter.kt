package com.kruu.kotlinsample.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kruu.kotlinsample.R
import com.kruu.kotlinsample.entities.Employees

class EmployeeListAdapter// Provide a suitable constructor (depends on the kind of dataset)
    (private val mDataset: MutableList<Employees>, private val context: Context) :
    RecyclerView.Adapter<EmployeeListAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        // each data item is just a string in this case
        var tvName: TextView
        var tvAge: TextView
        var tvSalary: TextView

        init {
            tvName = view.findViewById(R.id.tvEmployeeName) as TextView
            tvAge = view.findViewById(R.id.tvAge) as TextView
            tvSalary = view.findViewById(R.id.tvSalary) as TextView
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmployeeListAdapter.MyViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.employees_list_layout, parent, false) as View


        return MyViewHolder(v)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvName.text = mDataset[position].employee_name
        holder.tvAge.text = mDataset[position].employee_age
        holder.tvSalary.text = mDataset[position].employee_salary
        holder.view.setOnClickListener {

        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return mDataset.size
    }
}
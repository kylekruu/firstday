package com.kruu.kotlinsample.entities

import com.kruu.kotlinsample.service.HelloService
import com.kruu.kotlinsample.service.impl.HelloServiceImpl
import io.reactivex.schedulers.Schedulers.single
import org.koin.dsl.module
import java.lang.reflect.Array.get

data class HelloMessageData(val message : String = "Hello Koin!")

val helloModule = module {

    single { HelloMessageData() }

    single { HelloServiceImpl(get()) as HelloService }
}
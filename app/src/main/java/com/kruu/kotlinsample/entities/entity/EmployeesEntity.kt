package com.kruu.kotlinsample.entities.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employeesentity")
data class EmployeesEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "employee_name") val employee_name: String?,
    @ColumnInfo(name = "employee_age") val employee_age: String?,
    @ColumnInfo(name = "employee_salary") val employee_salary: String?,
    @ColumnInfo(name = "profile_image") val profile_image: String?
)
package com.kruu.kotlinsample.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Employees {

    @SerializedName("data")
    val employees: List<Employees>? = null
    @SerializedName("id")
    @Expose
    val id: Int = 0
    @SerializedName("employee_name")
    @Expose
    val employee_name: String? = null
    @SerializedName("employee_salary")
    @Expose
    val employee_salary:  String? = null
    @SerializedName("employee_age")
    @Expose
    val employee_age: String? = null
    @SerializedName("profile_image")
    @Expose
    val profile_image: String? = null

}
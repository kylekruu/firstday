package com.kruu.kotlinsample.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kruu.kotlinsample.entities.Employees
import com.kruu.kotlinsample.interfaces.APIHandler
import com.kruu.kotlinsample.network.ServiceGenerator
import com.kruu.kotlinsample.repository.EmployeesRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MainViewModel : ViewModel()  {

    //create a new Job
    private val parentJob = Job()
    //create a coroutine context with the job and the dispatcher
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    //create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)
    //initialize news repo
    private val employeesRepository: EmployeesRepository = EmployeesRepository(ServiceGenerator.makeRetrofitService())
    //live data that will be populated as news updates
    val employeesLiveData = MutableLiveData<MutableList<Employees>>()
    fun getEmployees() {
        ///launch the coroutine scope
        scope.launch {
            //get latest news from news repo
            val employees = employeesRepository.getEmployees()
            //post the value inside live data
            employeesLiveData.postValue(employees)

        }
    }
    fun cancelRequests() = coroutineContext.cancel()



}
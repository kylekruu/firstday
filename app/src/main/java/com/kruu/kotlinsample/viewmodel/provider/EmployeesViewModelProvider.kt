package com.kruu.kotlinsample.viewmodel.provider

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kruu.kotlinsample.viewmodel.MainViewModel

@Suppress("UNCHECKED_CAST")
class EmployeesViewModelProvider : ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel() as T
    }
}
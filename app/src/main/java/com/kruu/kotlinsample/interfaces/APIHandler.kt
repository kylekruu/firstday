package com.kruu.kotlinsample.interfaces

import com.kruu.kotlinsample.entities.Employees
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


interface APIHandler {
    @GET("/api/v1/employees")
    suspend fun getUsers(): Response<Employees>
}
package com.kruu.kotlinsample.repository

import androidx.lifecycle.LiveData
import com.kruu.kotlinsample.dao.EmployeesDao
import com.kruu.kotlinsample.entities.Employees
import com.kruu.kotlinsample.interfaces.APIHandler

class EmployeesRepository(private val apiInterface: APIHandler) : BaseRepository() {

    //get latest news using safe api call
    suspend fun getEmployees() :  MutableList<Employees>?{
        return safeApiCall(
            //await the result of deferred type
            call = {apiInterface.getUsers()},
            error = "Error fetching employees"
            //convert to mutable list
        )?.employees?.toMutableList()
    }

}
package com.kruu.kotlinsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.kruu.kotlinsample.adapter.EmployeeListAdapter
import com.kruu.kotlinsample.database.AppDatabase
import com.kruu.kotlinsample.entities.entity.EmployeesEntity
import com.kruu.kotlinsample.entities.helloModule
import com.kruu.kotlinsample.viewmodel.MainViewModel
import com.kruu.kotlinsample.viewmodel.provider.EmployeesViewModelProvider
import kotlinx.coroutines.*
import org.koin.core.context.startKoin

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: MainViewModel
    private var myJob: Job? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //INITIALIZATION
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "employees"
        ).build()
        val viewModelFactory = EmployeesViewModelProvider()
        val rvEmployees = findViewById<RecyclerView>(R.id.rvEmployees)

        //SHOW KOIN DIALOG
        showKoin()


        //Use view ModelFactory to initialize view model
        viewModel =  ViewModelProviders.of(this@MainActivity, viewModelFactory)
            .get(MainViewModel::class.java)
        //get latest news from view model
        viewModel.getEmployees()
        //observe viewModel live data
        viewModel.employeesLiveData.observe(this, Observer {
            //bind your ui here
            rvEmployees.apply {
                // set a LinearLayoutManager to handle Android
                // RecyclerView behavior
                layoutManager = LinearLayoutManager(applicationContext)
                // set the custom adapter to the RecyclerView
                adapter = EmployeeListAdapter(it, applicationContext)
            }
            GlobalScope.launch {
                it.forEach {
                    db.userDao().insert(EmployeesEntity(it.id,it.employee_name,it.employee_age,
                        it.employee_salary,it.profile_image))
                    println("Inserted")
                }
            }
        })

    }

    fun showKoin() {
        startKoin {
            // use Koin logger
            printLogger()
            // declare modules
            modules(helloModule)
        }

        KoinApplication().sayHello(applicationContext)
    }

    override fun onDestroy() {
        myJob?.cancel()
        super.onDestroy()
    }
}

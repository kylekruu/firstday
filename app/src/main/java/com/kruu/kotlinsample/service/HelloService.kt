package com.kruu.kotlinsample.service

interface HelloService {
    fun hello(): String
}
package com.kruu.kotlinsample.service.impl

import com.kruu.kotlinsample.entities.HelloMessageData
import com.kruu.kotlinsample.service.HelloService

class HelloServiceImpl(private val helloMessageData: HelloMessageData) : HelloService {

    override fun hello() = "Hey, ${helloMessageData.message}"
}
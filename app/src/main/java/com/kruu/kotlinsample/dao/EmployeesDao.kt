package com.kruu.kotlinsample.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kruu.kotlinsample.entities.entity.EmployeesEntity

@Dao
interface EmployeesDao {

    @Query("SELECT * from employeesentity ORDER BY id ASC")
    fun getEmployees(): LiveData<List<EmployeesEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(employeesEntity: EmployeesEntity)


}
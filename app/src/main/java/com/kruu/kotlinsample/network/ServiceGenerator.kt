package com.kruu.kotlinsample.network

import com.google.gson.GsonBuilder
import com.kruu.kotlinsample.interfaces.APIHandler
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


object ServiceGenerator {
    const val BASE_URL = "http://dummy.restapiexample.com"

    fun makeRetrofitService(): APIHandler {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(APIHandler::class.java)
    }
}
package com.kruu.kotlinsample

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kruu.kotlinsample.service.HelloService
import org.koin.core.KoinComponent
import org.koin.core.inject
import android.R
import android.widget.Toast
import androidx.appcompat.view.ContextThemeWrapper


class KoinApplication : KoinComponent {

    // Inject HelloService
    val helloService by inject<HelloService>()

    // display our data
    fun sayHello(context: Context){
        Toast.makeText(context,  "This message is made by Koin", Toast.LENGTH_LONG).show()
    }
}
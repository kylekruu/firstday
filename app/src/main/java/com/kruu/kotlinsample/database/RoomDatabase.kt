package com.kruu.kotlinsample.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kruu.kotlinsample.dao.EmployeesDao
import com.kruu.kotlinsample.entities.entity.EmployeesEntity

@Database(entities = arrayOf(EmployeesEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): EmployeesDao
}